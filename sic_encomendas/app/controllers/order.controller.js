const Order = require('../models/order.model.js');
const Item = require('../models/item.model');
const backOfficeAPI = require("../../service/backoffice.service");
const https = require('https');
const request = require('request');
const axios = require('axios');

// Create and Save a new Order
exports.create = async (req, res) => {
    try{
        // ************************************************************
        // ********************************** CONECT TO BACKOFFICE ****
        // ***************************************** BEGIN ************

        req.body.items.forEach(it => {
            delete it._id;
            delete it.order;
        });

        const data = JSON.stringify(req.body);

        const options = {
            hostname: 'sic-bo.azurewebsites.net',
            path: '/api/validate',
            method: 'POST',
            strictSSL: false,
            "rejectUnauthorized": false,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        };

        const reqe = await https.request(options, async (resp) => {
            console.log('statusCode: ', resp.statusCode);

            let body ='';
            resp.on('data', async (d) => {
                await process.stdout.write(d);
                body += d;

            });

            resp.on('end', async () => {

                if(resp.statusCode !== 200){
                    res.status(resp.statusCode).send(body);
                } else {

                    // Create an empty Order and save it in the database
                    const order = await new Order().save();
                    const items = req.body.items;
                    items.forEach(async (item) => {

                        // console.log('ITEM: ', item);

                        // Create a new Item
                        const newItem = await new Item(item);

                        // console.log('ITEM: ', newItem);

                        // Include Item in the order
                        order.items.push(newItem);
                        // Associate Order Id with the new Item
                        newItem.order = order;
                        // Save the new item into Mongo
                        await newItem.save();
                    });
                    await order.save();
                    res.status(201).send(order);
                }
            });

        });
        reqe.on('error', (error) => {
            console.error(error)
        });

        reqe.write(data);
        reqe.end();

        // ***************************************** END **************
        // ************************************************************

    } catch(err) {
        res.status(400).send({
            message: err.message || "Some error occurred while creating the Order."
        });
    }
};

// Retrieve and return all orders from the database
exports.findAll = async (req, res) => {
    try{
        const order = await Order.find().populate({path: 'items', populate: {path: 'item'}});
        res.status(200).json(order);
    } catch(err){
        res.status(400).send({
            message : err.message || "Some error occurred while retrieving orders."
        });
    }

};

exports.findOne = async (req, res) => {
    try {
        const { orderId } = req.value.params;
        const order = await Order.findById(orderId).populate({path: 'items', populate: {path: 'item'}});
        res.status(200).json(order);
    } catch(err) {
        if(err.kind === "ObjectId") {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        return res.status(400).send({
            message: "Error retrieving order with id " + req.params.orderId
        });
    }

};

// Update a order identified by the orderId in the request
exports.update = async (req, res) => {
    try {
        const { orderId } = req.value.params;
        const order = await Order.findById(orderId);
        if(!order) {
            return res.status(404).send({
                message: "Order not found with id " + req.value.params
            });
        }
        // ************************************************************
        // ********************************** CONECT TO BACKOFFICE ****
        // ***************************************** BEGIN ************

        req.body.items.forEach(it => {
            delete it._id;
            delete it.order;
        });

        const data = JSON.stringify(req.body);

        const options = {
            hostname: 'sic-bo.azurewebsites.net',
            path: '/api/validate',
            method: 'POST',
            strictSSL: false,
            "rejectUnauthorized": false,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        };

        const reqe = await https.request(options, async (resp) => {
            console.log('statusCode: ', resp.statusCode);

            let body ='';
            resp.on('data', async (d) => {
                await process.stdout.write(d);
                body += d;

            });

            resp.on('end', async () => {

                if(resp.statusCode !== 200){
                    res.status(resp.statusCode).send(body);
                } else {

                    const aux = [];
                    req.body.items.forEach(async (item) => {
                        // Create a New Item
                        const newItem = new Item(item);
                        // Associate the order Id to the Item
                        newItem.order = orderId;
                        // Add the Item to the assistant array
                        aux.push(newItem);
                        // Save the item into Mongo
                        await newItem.save();
                    });
                    // Replace the Items in the order with the assistant array
                    order.items = aux;
                    // Save the order update
                    await order.save();

                    res.status(200).send(order);
                }
            });

        });
        reqe.on('error', (error) => {
            console.error(error)
        });

        reqe.write(data);
        reqe.end();

        // ***************************************** END **************
        // ************************************************************

    } catch(err) {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        return res.status(500).send({
            message: "Error updating order with id " + req.params.orderId,
            error: err.message
        });
    }

};

// delete a order with the specified orderid in the request
exports.delete = async (req, res) => {
    try {
        const { orderId } = req.value.params;
        // Find the order by its Id and delete it from the Mongo
        const order = await Order.findByIdAndRemove(orderId);
        // With the items list in the deleted order, delete items from Mongo
        order.items.forEach(async (item) =>{
            var item = await Item.findByIdAndRemove(item);
        });
        res.send({message: "Order deleted successfully!"});
    } catch(err) {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        return res.status(500).send({
            message: "Could not delete order with id " + req.params.orderId
        });
    }

};

exports.getItems = async (req, res) => {
    try{
        const { orderId } = req.value.params;
        const order = await Order.findById(orderId).populate({path: 'items', populate: {path: 'item'}});
        res.status(200).json(order.items);

    } catch(err) {
        res.status(400).send({
            message : err.message || "Some error occurred while retrieving items."
        });
    }
};

exports.newItem = async (req, res) => {
    try {


        // ************************************************************
        // ********************************** CONECT TO BACKOFFICE ****
        // ***************************************** BEGIN ************

        const data = JSON.stringify(req.body);

        req.body.items.forEach(it => {
            delete it._id;
            delete it.order;
        });

        const options = {
            hostname: 'sic-bo.azurewebsites.net',
            path: '/api/validate',
            method: 'POST',
            strictSSL: false,
            "rejectUnauthorized": false,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        };

        const reqe = await https.request(options, async (resp) => {
            console.log('statusCode: ', resp.statusCode);

            let body ='';
            resp.on('data', async (d) => {
                await process.stdout.write(d);
                body += d;

            });

            resp.on('end', async () => {

                if(resp.statusCode !== 200){
                    res.status(resp.statusCode).send(body);
                } else {

                    const { orderId } = req.value.params;
                    const order = await Order.findById(orderId);

                    req.body.items.forEach(async (item) => {
                        // Create a new Item
                        const newItem = new Item(item);
                        // Assign Item to the order
                        newItem.order = order;
                        // Add Item to Order
                        order.items.push(newItem);
                        // Save the Item
                        await newItem.save();
                    });
                    // Save the Order
                    await order.save();

                    res.status(201).json(order);
                }
            });

        });
        reqe.on('error', (error) => {
            console.error(error)
        });

        reqe.write(data);
        reqe.end();

        // ***************************************** END **************
        // ************************************************************

    } catch(err) {
        if(err.kind === "ObjectId") {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        return res.status(400).send({
            message: err.message || "Some error occurred adding new item to order id " + req.params.orderId
        });
    }
};

exports.findOneItem = async (req, res) => {
    try {
        const orderId = req.params.orderId;
        const order = await Order.findById(orderId);
        if(!order){
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        const itemId = req.params.itemId;
        const item = await Item.findById(itemId);
        if(!item){
            return res.status(404).send({
                message: "Item not found with id " + req.params.itemId
            });
        }
        res.status(200).json(item);
    } catch(err) {
        return res.status(400).send({
            message: err.message || "Some error occurred retrieving the item with id " + req.params.itemId
        });
    }
};

exports.deleteOneItem = async (req, res) => {
    try {
        const itemId = req.params.itemId;
        const orderId = req.params.orderId;

        // Finds the item to delete
        const itemToDelete = await Item.findById(itemId);
        if(!itemToDelete) {
            return res.status(404).send({
                message: "Item not found with the id " + req.params.itemId
            });
        }
        // Finds the order which has the item who will be delete
        const order = await Order.findById(orderId).populate({path: 'items', populate: {path: 'item'}});
        if(!order) {
            return res.status(404).send({
                message: "Order not found with the id " + req.params.orderId
            });
        }

        const vec = [itemToDelete.ProductId];
        let count = 0;

        while(count < vec.length) {
            order.items.forEach((item) => {
                if(item.ParentId === vec[count]){
                    vec.push(item.ProductId);
                }
            });
            count++;
        }

        order.items.forEach(async (item, index) => {
            if(vec.includes(item.ProductId)){
                await Item.findByIdAndRemove(item._id);
                order.items.splice(index, 1);
            }
        });
        await order.save();
        res.status(200).json("Item(s) deleted ids: " + vec);
    } catch(err) {
        return res.status(400).send({
            message: err.message || "Some error occurred deleting the item with id " + req.params.itemId
        })
    }
};
