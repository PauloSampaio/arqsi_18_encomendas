const https = require('https');

exports.findAll = async (req, res) => {
    try {

        const options = {
            hostname: 'sic-bo.azurewebsites.net',
            path: '/api/catalog',
            method: 'GET',
            strictSSL: false,
            "rejectUnauthorized": false,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const reqe = await https.request(options, async (resp) => {
            console.log('statusCode: ', resp.statusCode);

            let body ='';
            resp.on('data', async (d) => {
                await process.stdout.write(d);
                body += d;
            });

            resp.on('end', async () => {
                res.status(200).send(body);
            });
        });

        reqe.on('error', (error) => {
            console.error(error)
        });

        // reqe.write(body);
        reqe.end();


    } catch(err) {
        res.status(400).send({
            message : err.message || "Some error occurred while retrieving catalogs."
        });
    }
};


exports.findProductsByCatalogId = async (req, res) => {

    try {

        const url = 'https://sic-bo.azurewebsites.net/api/catalog/' + req.params.catalogId + '/products';

        https.get(url, resp => {
            resp.setEncoding("utf8");
            let body = "";
            resp.on("data", data => {
                body += data;
            });
            resp.on("end", () => {
                body = JSON.parse(body);
                // console.log(body);
                res.status(200).send(body);
            });
        });

    } catch(err) {
        res.status(400).send({
            message : err.message || "Some error occurred while retrieving products."
        });
    }
};

