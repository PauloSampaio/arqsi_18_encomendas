const Item = require('../models/item.model');

module.exports = {
    findAll: async (req, res) => {
        const items = await Item.find();
        res.status(200).json(items);
    },

    findOne: async (req, res) => {
        try {
            const itemId = req.params.itemId;
            const item = await Item.findById(itemId);
            res.status(200).json(item);
        } catch(err) {
            if(err.kind === "ObjectId") {
                return res.status(404).send({
                    message: "Item not found with id " + req.params.itemId
                });
            }
            return res.status(400).send({
                error: err.message,
                message: "Error retrieving item with id " + req.params.itemId
            });
        }
    }
    /*create: async (req, res) => {
        try{
            const order = await Order.findById(req.body.order);

            const newItem = req.body;
            delete newItem.order;

            const item = new Item(newItem);
            item.order = order;
            await item.save();

            order.item.push(item);
            await order.save();

            res.status(200).json(item);
        } catch(err) {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Item."
            });
        }
    }*/
};