const https = require('https');

exports.findDimensions = async (req, res) => {

    try{
        const url = 'https://sic-bo.azurewebsites.net/api/dimensions';

        https.get(url, resp => {
            resp.setEncoding("utf8");
            let body = "";
            resp.on("data", data => {
                body += data;
            });
            resp.on("end", () => {
                body = JSON.parse(body);
                // console.log(body);
                res.status(200).send(body);
            });
        });

    } catch(err) {
        res.status(400).send({
            message : err.message
        });
    }
};