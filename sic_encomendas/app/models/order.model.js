const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({

    items: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Item' }]
}, {
    timestamps: true
});

module.exports = mongoose.model('Order', OrderSchema);
