const mongoose = require('mongoose');

const ItemSchema = mongoose.Schema({
    ProductId: Number,
    MaterialId:  Number,
    FinishId: Number,
    Length: Number,
    Width: Number,
    Height: Number,
    ParentId: Number,
    IsRequired: Number,
    order: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Order'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Item', ItemSchema);