const express = require('express');
const MaterialController = require('../controllers/material.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(MaterialController.findMaterials);

module.exports = router;

