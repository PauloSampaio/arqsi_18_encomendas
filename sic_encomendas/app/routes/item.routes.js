const router = require('express-promise-router')();

const ItemController = require('../controllers/item.controller');
const { validateParams, schemas } = require('../../helpers/routeHelpers');

router.route('/').get(ItemController.findAll);
    //.post(ItemController.create)


router.route('/:itemId').get(validateParams(schemas.idSchema, 'itemId'),ItemController.findOne);

module.exports = router;
