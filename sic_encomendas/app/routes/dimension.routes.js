const express = require('express');
const DimensionController = require('../controllers/dimension.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(DimensionController.findDimensions);

module.exports = router;

