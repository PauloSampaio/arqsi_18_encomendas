const express = require('express');
const CatalogsController = require('../controllers/catalog.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(CatalogsController.findAll);

router.route('/:catalogId')
    .get(CatalogsController.findProductsByCatalogId);

module.exports = router;

