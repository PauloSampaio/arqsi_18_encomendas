const express = require('express');
const OrdersController = require('../controllers/order.controller');
const CatalogsController = require('../controllers/catalog.controller');
const router = require('express-promise-router')();
const { validateParams, validateBody, schemas } = require('../../helpers/routeHelpers');

router.route('/')
    // Create a new Order
    .post(/*validateBody(schemas.orderSchema),*/ OrdersController.create)
    // Retrieve all Orders
    .get(OrdersController.findAll);

router.route('/:orderId')
    // Retrieve a single Order with orderId
    .get(validateParams(schemas.idSchema, 'orderId'), OrdersController.findOne)
    // Update a Order with orderId, replace the actual items list, for a new one
    .put(validateParams(schemas.idSchema, 'orderId'), /*validateBody(schemas.orderSchema),*/ OrdersController.update)
    // Delete a Order with orderId and its items
    .delete(validateParams(schemas.idSchema, 'orderId'), OrdersController.delete);

router.route('/:orderId/items')
    // Retrieve all order's items with orderId
    .get(validateParams(schemas.idSchema, 'orderId'), OrdersController.getItems)
    // Append a new item to the order with orderId
    .post(validateParams(schemas.idSchema, 'orderId'), OrdersController.newItem);

router.route('/:orderId/items/:itemId')
    .get(validateParams(schemas.idSchema, 'orderId'), validateParams(schemas.idSchema, 'itemId'), OrdersController.findOneItem)
    .delete(validateParams(schemas.idSchema, 'orderId'), validateParams(schemas.idSchema, 'itemId'), OrdersController.deleteOneItem);

module.exports = router;
