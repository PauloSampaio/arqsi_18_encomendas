const express = require('express');
const ProductController = require('../controllers/product.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(ProductController.findProducts);

module.exports = router;

