const express = require('express');
const CategoryController = require('../controllers/category.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(CategoryController.findCategories);

module.exports = router;

