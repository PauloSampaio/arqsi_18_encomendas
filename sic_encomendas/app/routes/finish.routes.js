const express = require('express');
const FinishController = require('../controllers/finish.controller');
const router = require('express-promise-router')();

router.route('/')
    .get(FinishController.findFinishes);

module.exports = router;

