const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const cors = require('cors');

// create express app
const app = express();

app.use(logger('dev'));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// Configuring the database
mongoose.Promise = global.Promise;

//Enable CORS policy
app.use(cors());

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
const router = express.Router();
app.use('/api', router);

const OrderRouter = require('./app/routes/order.routes');
app.use('/api/orders', OrderRouter);

const ItemRouter = require('./app/routes/item.routes');
app.use('/api/items', ItemRouter);

const CatalogRouter = require('./app/routes/catalog.routes');
app.use('/api/catalogs', CatalogRouter);

const MaterialRouter = require('./app/routes/materials.routes');
app.use('/api/material', MaterialRouter);

const ProductRouter = require('./app/routes/product.routes');
app.use('/api/product', ProductRouter);

const CategoryRouter = require('./app/routes/category.routes');
app.use('/api/category', CategoryRouter);

const FinishRouter = require('./app/routes/finish.routes');
app.use('/api/finish', FinishRouter);

const DimensionRouter = require('./app/routes/dimension.routes');
app.use('/api/dimension', DimensionRouter);


// *********************************************************************
 /*const request = require('request');

request('https://sic-bo.azurewebsites.net/api/product', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    //console.log(body.url);
    //console.log(body.explanation);
    console.log(body[0]);
});*/