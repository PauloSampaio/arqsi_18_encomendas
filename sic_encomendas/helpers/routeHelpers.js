const Joi = require('joi');

module.exports = {
    validateParams: (schema, name) => {
        return (req, res, next) => {
            const result = Joi.validate({ param: req['params'][name] }, schema);
            if (result.error) {
                return res.status(400).json(result.error);
            } else {
                if (!req.value)
                    req.value = {};

                if (!req.value['params'])
                    req.value['params'] = {};

                req.value['params'][name] = result.value.param;
                next();
            }
        }
    },

    validateBody: (schema) => {
        return (req, res, next) => {
            const result = Joi.validate( req.body.items, schema);
            if (result.error) {
                console.log("ERROR: "+result.error.message);
                return res.status(400).json(result.error);
            } else {

                if (!req.value)
                    req.value = {};

                if (!req.value['body'])
                    req.value['body'] = {};

                req.value['body'] = result.value;
                next();
            }
        }
    },

    schemas: {

        itemSchema: Joi.object().keys({
            ProductId: Joi.number().required(),
            MaterialId: Joi.number().required(),
            FinishId: Joi.number().required(),
            Length: Joi.number().integer().min(0).required(),
            Width: Joi.number().integer().min(0).required(),
            Height: Joi.number().integer().min(0).required(),
            ParentId: Joi.number().integer(),
            IsRequired: Joi.number().integer().min(0).max(1).required()
        }),
            /*
            const objectSchema = joi.object({
              source: joi.string().min(1).required(),
              path: joi.string().min(1).required()
            }).required();

            const arraySchema = joi.array().items(objectSchema).min(1).unique()
            .required();


            name: Joi.string().required(),
            password: Joi.string().required(),
            email: Joi.string().email().required(),
            admin: Joi.boolean().required(),
            medico: Joi.boolean().required(),
            farmaceutico: Joi.boolean().required(),
            utente: Joi.boolean().required()
            */


        idSchema: Joi.object().keys({
            param: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        })
     }
};

/*
const orderSchema = Joi.object({
    items: Joi.array().items(itemSchema)
});

module.exports = orderSchema;
*/